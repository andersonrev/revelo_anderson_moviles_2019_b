package com.example.adivina

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var gameScoreTextView: TextView

    private lateinit var tapMeButton: Button
    private lateinit var startButton: Button


    private lateinit var countDownTimer: CountDownTimer
    private var initialCountDown: Long = 10000
    private var countDownInterval: Long = 1000
    private var timeLeft = 10

    private var gameScore = 0





    private var isGameStarted = false
    private val TAG = MainActivity::class.java.simpleName

    private var n = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Log.d(TAG, "on Create called, score is $gameScore")



        gameScoreTextView = findViewById(R.id.tv_puntajeTotal)
        startButton = findViewById(R.id.idstart)
        tapMeButton = findViewById(R.id.tab_button)


        startButton.setOnClickListener {_ -> startGame()
        }
        tapMeButton.setOnClickListener {_ -> incrementScore(10-timeLeft,n)
        }
    }

    private fun startGame(){
        restartTimeGame()
        countDownTimer = object : CountDownTimer(initialCountDown,countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft = millisUntilFinished.toInt()/1000
            }
            override fun onFinish() {
                // Toast.makeText(this,"Too slow baby",Toast.LENGTH_LONG).show()
            }
        }
        countDownTimer.start()

    }

    private fun restartTimeGame(){
        n = (1..10).shuffled().first()
        val randTime = getString(R.string.Time , Integer.toString(n))

    }

    private fun incrementScore(tapTime: Int, randomTime: Int){
        if(tapTime == randomTime){
            gameScore = gameScore +100;
        }
        else if(Math.abs(tapTime-randomTime)==1){
            gameScore = gameScore + 50;
        }
        actualizarScoreView()
    }

    private fun actualizarScoreView(){
        val totalPoints = getString(R.string.ScoreTotal , Integer.toString(gameScore))
        gameScoreTextView.text = totalPoints
    }
}
